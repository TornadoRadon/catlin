package uz.softex.catlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CatlinApplication

fun main(args: Array<String>) {
    runApplication<CatlinApplication>(*args)
}
